from aiogram.dispatcher.filters.state import StatesGroup, State


class AddFile(StatesGroup):
    ID = State()


class GetFile(StatesGroup):
    NAME = State()


class DelFile(StatesGroup):
    NAME = State()


class SendFile(StatesGroup):
    USERNAME = State()
    FILENAME = State()
