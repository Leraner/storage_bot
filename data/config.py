from pathlib import Path

from environs import Env

env = Env()
env.read_env()

# Bot
BOT_TOKEN = env.str("BOT_TOKEN")
ADMINS = env.list("ADMINS")
db_user = env.str("DB_USER")
db_pass = env.str("DB_PASS")
db_name = env.str('DB_NAME')
host = env.str('HOST')

# Dirs
BASE_DIR = Path(__file__).parent
MEDIA_DIR = BASE_DIR / 'media'
