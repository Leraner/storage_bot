import datetime

from aiogram import types
from gino.schema import GinoSchemaVisitor

from .config import db_name, db_pass, db_user, host
from .models import db, User, File


class DBCommands:
    async def get_user(self, user_id):
        user = await User.query.where(User.user_id == user_id).gino.first()
        return user

    async def get_all_files(self):
        user = types.User.get_current()
        files = await File.query.where(File.user_id == user.id).gino.all()
        return files

    async def get_file(self, name, user_id):
        all_files = await File.query.where(File.name == name).gino.all()
        for file in all_files:
            if file.user_id == user_id:
                return file

    async def delete_file(self, name, user_id):
        all_files = await File.query.where(File.name == name).gino.all()
        for file in all_files:
            if file.user_id == user_id:
                file = await File.delete.where(File.file_id == file.file_id).gino.first()
                return file

    async def create_user(self):
        user = types.User.get_current()
        old_user = await self.get_user(user.id)

        if old_user:
            return old_user

        new_user = User()
        new_user.username = user.username
        new_user.user_id = user.id
        new_user.last_name = user.last_name
        new_user.first_name = user.first_name

        await new_user.create()
        return new_user

    async def create_file(self, name, file_id, file_unique_id, file_size, user_id):
        new_file = File()
        new_file.name = name
        new_file.file_id = file_id
        new_file.file_unique_id = file_unique_id
        new_file.file_size = file_size
        new_file.date = datetime.datetime.strptime(
            datetime.datetime.now().strftime("%Y/%m/%d %H:%M"),
            "%Y/%m/%d %H:%M")
        new_file.user_id = user_id

        await new_file.create()
        return new_file


async def create_db():
    await db.set_bind(f'postgresql+asyncpg://{db_user}:{db_pass}@{host}/{db_name}')
    db.gino: GinoSchemaVisitor
    # await db.gino.drop_all()
    await db.gino.create_all()
