from gino import Gino
from sqlalchemy import (
    Column, Integer, BigInteger, String, Sequence, ForeignKey, TIMESTAMP
)
from sqlalchemy import sql
from sqlalchemy.orm import relationship

db = Gino()


class File(db.Model):
    """Таблица фалов"""
    __tablename__ = 'File'
    id = Column(Integer, Sequence('parler_id_seq'), unique=True, primary_key=True)
    name = Column(String)
    file_id = Column(String)
    file_unique_id = Column(String)
    file_size = Column(BigInteger)
    date = Column(TIMESTAMP)
    user_id = Column(Integer, ForeignKey('User.user_id'), unique=False)
    query: sql.Select


class User(db.Model):
    """Таблица пользователей"""
    __tablename__ = 'User'
    id = Column(Integer, Sequence('user_id_seq'), unique=True)
    username = Column(String(30), nullable=False)
    user_id = Column(BigInteger, primary_key=True)
    last_name = Column(String(30), nullable=True)
    first_name = Column(String(30), nullable=True)
    file = relationship('File', backref='user', lazy='dynamic')
    query: sql.Select
