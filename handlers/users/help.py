from aiogram import types
from aiogram.dispatcher.filters.builtin import CommandHelp

from loader import dp


@dp.message_handler(CommandHelp())
async def bot_help(message: types.Message):
    text = ("Список команд: ",
            "/add - Добавить файл (максимальный размер файла - 2ГБ)",
            "/del - Удалить файл",
            "/ls - Посмотреть все файлы",
            "/send - Отправить файл пользователю, который писал боту",
            "/help - Получить справку")
    
    await message.answer("\n".join(text))
