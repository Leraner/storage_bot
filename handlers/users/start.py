from aiogram import types
from aiogram.dispatcher.filters.builtin import CommandStart

from data import database
from loader import dp


db = database.DBCommands()


@dp.message_handler(CommandStart())
async def bot_start(message: types.Message):
    await db.create_user()
    await message.answer(f"Привет, {message.from_user.full_name}!")
