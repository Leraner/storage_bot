from .help import dp
from .start import dp
from .echo import dp
from .file import dp

__all__ = ["dp"]
