from aiogram import types
from aiogram.dispatcher import FSMContext

from data import database
from data.models import User
from loader import dp
from states.file import AddFile, GetFile, DelFile, SendFile

db = database.DBCommands()


@dp.message_handler(commands=['send'], state=None)
async def send_file(message: types.Message):
    await message.answer('Напишите username пользователя')
    await SendFile.USERNAME.set()


@dp.message_handler(state=SendFile.USERNAME)
async def get_username(message: types.Message, state: FSMContext):
    if '@' in message.text:
        username = message.text.rsplit('@')
    else:
        username = message.text

    user = await User.query.where(User.username == username).gino.first()
    if user:
        await message.answer('Введите имя файла')
        await state.update_data(user_id=user.user_id)
        await SendFile.FILENAME.set()
    else:
        await message.answer('Такого пользователя не существует')
        await state.finish()


@dp.message_handler(state=SendFile.FILENAME)
async def get_filename(message: types.Message, state: FSMContext):
    file = await db.get_file(name=message.text, user_id=message.from_user.id)
    if file:
        data = await state.get_data()
        user_id = data.get('user_id')
        await dp.bot.send_document(user_id, document=file.file_id)
        await message.answer('Файл отправлен')
    else:
        await message.answer('Такого файла не существует')

    await state.finish()


@dp.message_handler(commands=['del'], state=None)
async def get_specific_file(message: types.Message):
    await message.answer('Введите имя файла')
    await DelFile.NAME.set()


@dp.message_handler(state=DelFile.NAME)
async def get_file_from_db(message: types.Message, state: FSMContext):
    file = await db.delete_file(name=message.text, user_id=message.from_user.id)
    if not file:
        await message.answer('Файл удалён')
    else:
        await message.answer('Такого файла нет')
    await state.finish()


@dp.message_handler(commands=['get'], state=None)
async def get_specific_file(message: types.Message):
    await message.answer('Введите имя файла')
    await GetFile.NAME.set()


@dp.message_handler(state=GetFile.NAME)
async def get_file_from_db(message: types.Message, state: FSMContext):
    file = await db.get_file(name=message.text, user_id=message.from_user.id)
    if file:
        await dp.bot.send_document(message.from_user.id, document=file.file_id)
    else:
        await message.answer('Такого файла нет')
    await state.finish()


@dp.message_handler(commands=['ls'])
async def show_files(message: types.Message):
    files = await db.get_all_files()
    if len(files) > 0:
        files_names = []
        for file in files:
            files_names.append(file.name)

        await message.answer('\n'.join(files_names))
    else:
        await message.answer('У вас нет файлов, добавьте их с помощью /addfile')


@dp.message_handler(commands=['add'], state=None)
async def add_file(message: types.Message):
    await message.answer('Добавьте файл')
    await AddFile.ID.set()


@dp.message_handler(state=AddFile.ID, content_types=['document'])
async def get_file(message: types.Message, state: FSMContext):
    file = await db.create_file(
        name=message.document.file_name,
        file_id=message.document.file_id,
        file_unique_id=message.document.file_unique_id,
        file_size=message.document.file_size,
        user_id=message.from_user.id
    )
    if file:
        await message.answer('Файл добавлен')
    else:
        await message.answer('Файл не добавлен')
    await state.finish()
